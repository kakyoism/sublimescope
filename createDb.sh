#! /bin/sh

rootDir=${1:?"Usage: $0 rootDir [cscopeExe]"}
cscopeExe=$2
if [[ -z "${scopeExe}" ]]; then
	cscopeExe="/usr/local/bin/cscope"
fi

projDir="${rootDir}"

lockFile="${projDir}"/cscope.fin
srcListFile="${projDir}"/cscope.files
rm -f "${projDir}"/cscope.*

find "${rootDir}" -name "*.c" -o -name "*.cpp" -o -name "*.h" -o -name "*.hpp" -o -name "*.cxx" > "${srcListFile}"

sed 's/\(.*\)/"\1"/g' "${srcListFile}" > "${projDir}"/tmpfile && mv "${projDir}"/tmpfile "${srcListFile}"

"${cscopeExe}" -q -R -b -i "${srcListFile}" -f "${projDir}"/cscope.out

echo "Done. Removed tmp files to avoid freezing on Go-to-Anything."
rm "${srcListFile}"
touch "${lockFile}"
