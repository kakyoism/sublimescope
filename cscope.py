import sublime, sublime_plugin
import sys, subprocess, shlex, re, subprocess, multiprocessing, json
from os.path import join, exists, splitext, dirname, abspath, normpath, isdir

ScriptDir = abspath(dirname(__file__))
DefaultCodec = 'ascii'
IsPython3 = sys.version_info[0] > 2
ConfigFile = join(ScriptDir, 'config.json')
SearchRegEx = re.compile(r'^(?P<Path>[^.]+\.\S{,3})\s+(?P<Function>[\w<>]+)\s+(?P<Linenum>\d+)\s+(?P<Contents>.+)$')

def RunCommand(cmd):
	process = subprocess.Popen(cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=0)
	(stdOut, stdErr) = process.communicate()

	if IsPython3:
		stdOut = stdOut.decode(DefaultCodec, 'ignore')
		stdErr = stdErr.decode(DefaultCodec, 'ignore')
	return (stdOut, stdErr, process.returncode, ' '.join(cmd))

def ReadConfig():
	with open(ConfigFile) as f:
		cfgStr = f.read()
		config = json.loads(cfgStr)
	return config

def WriteConfig(config):
	outStr = json.dumps(config, indent=4)
	with open(ConfigFile, 'w') as f:
		f.write(outStr)
	
def CreateDbWorker(rootDir, cscopeExe):
	script = join(ScriptDir, 'createDb.sh')
	cmd = ['/bin/sh', script, rootDir, cscopeExe]
	print(' '.join(cmd))
	subprocess.call(cmd)

class CreateDbCommand(sublime_plugin.WindowCommand):
	"""Create or update cscope db for a code base. Call another script to avoid freezing up UI."""
	def run(self, dirs=None):
		print('CreateDb')
		config = ReadConfig()
		cscopeExe = config['cscopeExe']
		root = dirs[0]
		if not root is None:
			if not isdir(root):
				root = abspath(dirname(root))
			rootDir = root
		else:
			rootDir = config['RootDir']
		jobs = []
		p = multiprocessing.Process(target=CreateDbWorker, args=(rootDir, cscopeExe))
		jobs.append(p)
		p.start()
		print('CreateDb: started')

class SetActiveCodebaseCommand(sublime_plugin.WindowCommand):
	"""Create or update cscope db for a code base. Call another script to avoid freezing up UI."""
	def run(self, dirs=None):
		config = ReadConfig()
		if dirs is None:
			return
		rootDir = dirs[0]
		config['RootDir'] = rootDir
		WriteConfig(config)

class FindSymbolCommand(sublime_plugin.TextCommand):
	"""Search symbol by cscope-defined types: Symbol=0, GlobalDefinition=1, FunctionCallers=2, FunctionCallees=3, Text=4, egrepPattern=5, filename=7, headerDependent=8, symbolMutator=9, headerDependent does not work."""
	def run(self, edit, SearchType):
		blocks = [self.view.word(s) for s in self.view.sel()]
		selWord = self.view.substr(blocks[0])
		seedFile = self.view.file_name()
		
		config = ReadConfig()
		rootDir = config['RootDir']
		cscopeExe = config['cscopeExe']

		refFile = join(rootDir, 'cscope.out')
		lockFile = join(rootDir, 'cscope.fin')
		if not exists(lockFile):
			sublime.error_message('Failed to find cscope database integrity lock: {0}. Create it before searching for symbols.'.format(lockFile))
			return

		cmd = [cscopeExe, '-f', refFile, '-d', '-L', '-{0}'.format(SearchType), selWord]
		[out, err, ret, searchCmd] = RunCommand(cmd)
		outLines = out.split('\n')

		nTrailBlankLine = 1
		nMatches = len(outLines) - nTrailBlankLine

		Separator = '==============\n'
		searchCmdLines = ['Command: {0}'.format(' '.join(cmd))]
		seedFileLines = ['Seed File: {0}'.format(seedFile)]
		headlines = [Separator+'Search Result:\n'+Separator]
		epilog = [Separator+'Found matches: {0}'.format(nMatches)]
		outLines = headlines + searchCmdLines + seedFileLines + outLines + epilog + ['\n\n']
		out = '\n\n'.join(outLines)

		# Append result to a separate file.
		outFile = join(rootDir, 'found_symbols.cscsearch')
		openMode = 'w'
		if exists(outFile):
			openMode = 'a'
		with open(outFile, openMode) as f:
			f.write(out)
		win = self.view.window()
		win.open_file(outFile)

class SaveTraceCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		lines = [self.view.line(s) for s in self.view.sel()]
		selLine = self.view.substr(lines[0])
		print(selLine)

		config = ReadConfig()
		rootDir = config['RootDir']
		
		# Append result to a separate file.
		outFile = join(rootDir, 'traces.cscsearch')
		openMode = 'w'
		if exists(outFile):
			openMode = 'a'
		with open(outFile, openMode) as f:
			f.writelines(['\n', selLine, '\n'])
		win = self.view.window()
		win.open_file(outFile)

class GotoCommand(sublime_plugin.TextCommand):
	"""cscope search results under any search mode share the same layout."""
	def run(self, edit):
		lines = [self.view.line(s) for s in self.view.sel()]
		selLine = self.view.substr(lines[0])
		
		matchResult = SearchRegEx.match(selLine)
		path = matchResult.groupdict()['Path']
		lineNum = matchResult.groupdict()['Linenum']
		assert lineNum.isdigit(), 'lineNum is NaN: {0}'.format(lineNum)

		win = self.view.window()
		fileRow = path + ':{0}'.format(lineNum)
		win.open_file(fileRow, sublime.ENCODED_POSITION)

class GotoHeaderCommand(sublime_plugin.TextCommand):
	"""Assume header .h, .hpp, or .hxx file is under the same folder. Open that file."""
	def run(self, edit):
		blocks = [self.view.word(s) for s in self.view.sel()]
		selWord = self.view.substr(blocks[0])
		seedFile = self.view.file_name()
		
		HeaderExts = ['h', 'hpp', 'hxx']
		bn = splitext(seedFile)[0]
		header = 'Undefined'
		for h in HeaderExts:
			tmp = bn + '.{0}'.format(h)
			if exists(tmp):
				header = tmp
				break

		win = self.view.window()
		win.open_file(header)
