# README #

## Quick summary ##

Welcom to SublimeScope, a simple cscope integration plugin for Sublime Text (http://www.sublimetext.com), the open-source cross-platform text editor. 


## What is Cscope ##

cscope (http://cscope.sourceforge.net) is a open-source tool for browsing source code, which stood the tests for decades already. The most well-known feature of cscope is its symbol search, capable of searching in a large code base for symbols, definitions, callers, callees, and even variable mutators. The similar products include Visual Assit (http://www.wholetomato.com) for Microsoft Visual Studio on Windows. 

## Who needs cscope and why ##

Mac and Linux C++ developers who need to read large code bases to find symbols or parse dependency chains would find cscope handy.

## Why combining cscope with Submlime Text ##

cscope has a full-fledged CLI, TUI, and a Tcl/TK GUI (cbrowser). Third party integrations are available for VIM and EMACS. However, to the best of my knowledge, there are no known cscope integrations for modern GUI editors or IDEs. Sublime Text has been one of the most popular programmer's editor in recent years, which already comes with nifty code navigation features such as "Go to Everything" and a simple Python-based plugin architecture. It appears a good host for cscope.

**This plugin is mainly for Mac users for now.** Linux support could be added, with a little bit of effort. The same applies to Windows, however, on Windows, Visual Assits, and the open-source tool PhatStudio work equally well as cscope. To use cscope on Windows, one would need to compile using Cygwin or MingW, which is a big bag to take with it. So currently I see no compelling reasons to port it to Windows.

The long-term goal of this project is to add semi-automated code navigation features.

## Version ##
0.1.0 beta


## Summary of set up ##

You'll need to install the dependencies and the plugin, and configure the plugin before using it.

## Environment ##

Mac OS X (Tested on 10.9 Mavericks only)

## Dependencies ##

* Sublime Text: Tested only on 2.0.2(http://www.sublimetext.com/2)
* cscope: Install cscope via Homebrew (http://brew.sh). The executable is usually installed to `/usr/local/bin`.

```
#!bash

brew install cscope
```


## Deployment instructions ##

After downloading or syncing the repo, run `install.sh`. The plugin will be installed into the designated Sublime Text package folders. On Mac, this is usually
    
    ~/Library/Application Support/Sublime Text 2/Packages/cscope


## Configuration ##

Because Sublime Text can open multiple codebase in the same window, when using the plugin for the first time, you'll need to specify the codebase of interest. This can be done using the side-bar menu `Set As Active Codebase` on a source folder in the side bar. Alternatively, you can edit the configuration file `config.json` to replace the value of the field "RootDir" with the full path to the targe codebase. 

At any time, you can customize the keyboard shortcuts and menu items as you want, just as you would with other Sublime Text plugins. The configuration files are located under the aforementioned package folders:

* Keyboard shortcuts: `Default (OSX).sublime-keymap`, and corresponding Linux and Windows files.
* Menus: `Context.sublime-menu`, you may add program and sidebar menu configurations yourself, although I don't see a need at the moment myself.

## Features and Usage ##

After deployment and configuration are successful, you can open Sublime Text 2 and load your code base, assuming there is a single root folder. Then you can:

* **Create a new cscope symbol database**: Open a source folder. In the side bar, right click the root of the folder and click `cscope ... > Create Symbol Database`. Another process will start to create the database without blocking Sublime Text's UI. Before the database is created, any symbol-finding operation will fail and trigger an error dialog. You can issue the command to as many as projects as you want. But before working with cscope, you'll need to specify an active codebase.
* **Specify an active codebase**: There can be multiple codebases open in Sublime Text simultaneously; however, only one active project can exist at all times from cscope's perspective. So one of them must be set to be the active project. Open a source folder. In the side bar, right click the root of the folder and click `cscope ... > Set As Active Codebase`. 
* **Find and navigate to a symbol**: Open a source file, e.g., *.cpp, select any variable in the file, such as a function or a arbitrary variable name, then right-click the context menu item: `cscope ... > Find Symbol`. A new tab will open containing all the results, and also the underneath command line command using cscope; the result is saved in `found_symbols..cscsearch` by default. cscope supports a list of target types; choose the correct context menu item.
* **Go to symbol reference**: On seeing a search result, select a word, and then click the context menu item `cscope ... > Go to Selection`, you'll navigate to the corresponding line of the source code.
* **Go to header file**: Open a source file, then right-click and click `cscope ... > Go to Header. The corresponding header file under the same folder will open in a new tab.
* **Save trace**: For a specific code-reading task, you can note down and save the individual search results for future references. There is a context menu Save Selection as Trace, which will save or append the new result to a new text file (`trace.cscsearch`). 

Most commands can be triggered by built-in keyboard shortcuts, see the context menu for them.

** Tips **: Because all the results are just text files, you can make backups of those results the way you would with text files.

## Limitations ##

* Although cscope supports multiple programming languages, currently the integration only supports C/C++. It should be trivial to change the createDb.sh script to support whatever languages you use. But I have never tested anything other than C++.
* The integration assumes that the codebase of interest should have a single tree of source files, i.e., all source files should be under one root folder.
* It has only been tested on Mac OS X, although it should be trivial to port it to Linux.

## License ##

This plugin is distributed under the MIT license (http://en.wikipedia.org/wiki/Mit_license)

## Branching Convention ##

The `master` branch is the development branch. The stable branches will be called `release-x.x.x`. They will appear as the project moves forward.

## Contribution guidelines ##

I'm fairly new to BitBucket and don't know too much about the features and dynamics of pull/push requests yet. Please feel free to contact me about contribution if you find this plugin worth digging.
