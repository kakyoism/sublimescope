#! /bin/sh

ScriptDir=$( cd "`dirname $0`" && pwd )
src="cscope.py"
dest="${HOME}/Library/Application Support/Sublime Text 2/Packages/cscope"

mkdir -p "${dest}" &> /dev/null
cd "${ScriptDir}"
cp "${src}" *keymap createDb.sh *.json *menu "${dest}"/
cd - &> /dev/null
